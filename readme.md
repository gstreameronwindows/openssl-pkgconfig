This folder provides `pkg-config` .pc files for OpenSSL
on Windows. After installing OpenSSL, place this folder
in the `lib` folder of the install location.

Although the .pc prefix is set to `c:/bin/OpenSSL`, this path
will be overridden by `pkg-config` and set to the grandparent
folder.
